from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QBrush
from PyQt5.QtCore import Qt

class Gomoku(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Gomoku'
        self.left = 470
        self.top = 0
        self.width = 510
        self.height = 510
        self.initUI()
        
    def initUI(self):
        self.circles = []
        self.coordinate_p1 = []
        self.coordinate_p2 = []
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()



    

        
      
    def mousePressEvent(self, event):
        Position = event.pos()
        geometry = self.geometry()
        w = geometry.width()
        h = geometry.height()
        x_s = geometry.width() / 18
        y_s = geometry.height() / 18
        x = (Position.x() + (x_s / 2)) // x_s
        y = (Position.y() + (y_s / 2)) // y_s
        #print(Position.x(),Position.y())
        if not ({ 'x': x, 'y': y } in self.circles):
            self.circles.append({ 'x': x, 'y': y })
            if len(self.circles) % 2 == 1:
                self.coordinate_p1.append({'x':x, 'y':y})
                print(self.coordinate_p1)
            elif len(self.circles) % 2 == 0:
                self.coordinate_p2.append({'x':x, 'y':y})
            
        
        for x in range(18):
            for y in range(18):
                self.winVertical(x,y)
                self.winHorizontal(x,y)
                self.winDiagonal(x,y)
        #print(x,y)
        event.accept()
        self.update()
    def winVertical(self,x,y):
        win_v_p1 = 0
        win_v_p2 = 0
        for yy in range(5):
            if {'x':x, 'y':y+yy} in self.coordinate_p1:
                win_v_p1 += 1
                #print('vertical P1' +str(win_v_p1))
            if {'x':x, 'y':y+yy} in self.coordinate_p2:
                win_v_p2 +=1
                #print('vertical P2'+ str(win_v_p2))
            
            if win_v_p1 == 5:
                print('Player 1 wins by 5 consecutive rocks vertically')
                
            if win_v_p2 == 5:
                print('Plater 2 wins by 5 consecutive rocks vertically')
        
    def winHorizontal(self, x, y):
        win_h_p1 = 0
        win_h_p2 = 0
        for xx in range(5):
            if {'x':x+xx, 'y':y} in self.coordinate_p1:
                win_h_p1 += 1
            if {'x':x+xx, 'y':y} in self.coordinate_p2:
                win_h_p2 +=1
            if win_h_p1 == 5:
                print('Player 1 wins by 5 consecutive rocks horizontally')
            if win_h_p2 == 5:
                print('Plater 2 wins by 5 consecutive rocks horizontally')

    def winDiagonal(self, x, y):
        win_d_p1 = 0
        win_d_p2 = 0
        for dd in range(5):
            if {'x':x+dd, 'y':y-dd} in self.coordinate_p1:
                win_d_p1 += 1
                #print(win_d_p1)
            if {'x':x+dd, 'y':y-dd} in self.coordinate_p2:
                win_d_p2 +=1
                #print('p2'+str(win_d_p2))
            if win_d_p1 == 5:
                print('Player 1 wins by 5 consecutive rocks diagonally')
            if win_d_p2 == 5:
                print('Plater 2 wins by 5 consecutive rocks diagonally')
       
    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)
        geometry = self.geometry()


        w = self.geometry().width()
        h = self.geometry().height()
        x_s = w / 18
        y_s = h / 18

        for i in range(0, 19) :
            p.drawLine(i*w//18, 0, i*w//18, h)
            p.drawLine(0, i*h//18, w, i*h//18)


        h = True
        for c in self.circles:
            #print(c)
            if h:
                p.setBrush(QBrush(Qt.black, Qt.SolidPattern))
                h = False
            else:
                p.setBrush(QBrush(Qt.white, Qt.SolidPattern))
                h = True
            #print(x_s * c['x'] - 5, y_s * c['y'] - 5)

             
            p.drawEllipse(x_s * c['x'] - 5, y_s * c['y'] - 5, 10, 10)


app = QApplication([])
window = Gomoku()

window.circles = []
window.show()
app.exec_()
        
